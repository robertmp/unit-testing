const expect = require('chai').expect;
const { assert } = require('chai');
const should = require('chai').should();
const mylib= require('../src/mylib');

describe('Unit testing mylib.js', ()=>{
    it('Should return 2 when using sum functoin with a=1 b=1', ()=>{
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    })
    it('Should return 1 when using mul functoin with a=1 b=1', ()=>{
        const result = mylib.mul(1,1);
        expect(result).to.equal(1);
    })
    it('Should return 2 when using mul functoin with a=2 b=1', ()=>{
        const result = mylib.div(myvar2,myvar1);
        expect(result).to.equal(2);
    })
});

before(()=>{
    myvar= 0;
    myvar1 = 1;
    myvar2= 2;
    console.log('Before testing');
});
it('Assert foo is not bar', () => {
    assert('foo'!=='bar');
})
it('Myvar should exist', ()=>{
    should.exist(myvar);
})

it.skip('Random',() => expect(mylib.random()).to.above(0.5));
after(()=>{
    console.log('Finished');
})